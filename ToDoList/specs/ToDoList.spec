To Do List
=====================


Proje Yapilacak
----------------
tags:toDoList_proje

*Adrese gidilir
*Listenin bos oldugu kontrol edilir
*"Proje yapilacak" itemi eklenir
*Listede "Proje yapilacak" iteminin olduğu kontrol edilir


Birden Fazla Item Ekleme
----------------
tags:toDoList_item_ekleme

*Adrese gidilir
*Listede "Proje yapilacak" iteminin olduğu kontrol edilir
*"Bilgisayari al" itemini eklenir
*"Bilgisayari al" iteminin "Proje yapilacak" iteminin altina eklendigi kontrol edilir


Mark Etme
----------------
tags:toDoList_mark_etme

*Adrese gidilir
*Listede "Proje yapilacak" ve "Bilgisayari al" iteminin oldugu kontrol edilir "itemKontrolSatiri"
* "toListMark" elementi listede var mi
*"Proje yapilacak" yanindaki radio button’a tiklanir
*"Proje yapilacak" iteminin mark edildigi kontrol edilir "itemKontrolSatiri", "toList"


Mark Etmeme
---------------
tags:toDoList_mark_etmeme

*Adrese gidilir
*Listede marked edilmiş item olduğu kontrol edilir
*"Proje yapilacak" yanindaki radio button’a tiklanir
*"Proje yapilacak" iteminin mark edildigi kontrol edilir "itemKontrolSatiri", "toList"


Sil
--------------------
tags:toDoList_sil

*Adrese gidilir
*Listede "Proje yapilacak" ve "Bilgisayari al" iteminin oldugu kontrol edilir "itemKontrolSatiri"
*"Her seyi duzenli yap" itemi eklenir ve eklendigi kontrol edilir
*"Proje yapilacak" itemi icin "radioButton" butonuna tikla "itemKontrolSatiri"
*"Proje yapilacak" itemi icin "toListDelete" butonuna tikla "itemKontrolSatiri"