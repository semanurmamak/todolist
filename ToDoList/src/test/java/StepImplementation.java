import com.thoughtworks.gauge.Step;
import java.util.List;

public class StepImplementation extends BaseTest {

    @Step("Anasayfa acilir")
    public void anasayfayaGit() {
        driver.get("https://todomvc.com/examples/vue/");
        System.out.println("Anasayfa yuklendi");
    }

    @Step("<saniye> saniye bekle")
    public void waitElement(int key) throws InterruptedException {
        Thread.sleep(key * 1000);
        System.out.println(key + " saniye beklendi");
    }

    @Step("<key> elementi listede var mi")
    public void checkElementStep(String key) {
        try {
            findElement(key);
            System.out.println("Element var");
        } catch (Exception e) {
            System.out.println("Element yok");
        }
    }

    @Step("<key> elementinde <text> itemi var mi")
    public void checkElement(String key, String text) {
        try {
            findElement(key);
            System.out.println(text+ " Elementi var");
        } catch (Exception e) {
            System.out.println(text+ " Elementi yok");
        }
    }

    @Step("<key> tikla")
    public void elementTikla(String key) {
        clickElement(key);
        System.out.println(key + " tiklandi");
    }

    @Step("<key> alanina <text> itemi yazilir")
    public void newToDo(String key, String text){
        sendkeysElement(key, text);
        enterClick(key);
        System.out.println(text + " yazildi" );
    }

    @Step("<itemKontrolSatiri> item eklendigi kontrol edilir <yeniItem>")
    public void itemEklemeKontrol(String itemKontrol, String yeniItem) {
        assertControls(itemKontrol, yeniItem);
        System.out.println(yeniItem + " Elementi listede vardir");
    }

    @Step("<itemKontrolSatiri> listesinde <key1> itemi <key2> iteminin altinda mi")
    public void yerbulma(String key3, String key1, String key2) {

        int key1int = itemListesi(key3).indexOf(key1);
        int key2int = itemListesi(key3).indexOf(key2);

        if (key1int > key2int) {
            System.out.println("Altinda");
        } else {
            System.out.println("Uzerinde");
        }
    }

    @Step("<key1> mark <itemKontrolSatiri> edildi <key2>")
    public void markEtme(String key1, String key3, String key2) {
        System.out.println("<*");
        if (markEtmek(key1, key2, key3) == true) {
            System.out.println("<*<");
            System.out.println("Mark Edildi");
        }
        else System.out.println("Mark Edilmedi");
    }

    @Step("<Proje Yapilacak> radio butonuna <radioButton> tikla <itemKontrolSatiri>")
    public void radioButon(String key1, String key2, String key3) {
        String text = key1;
        List<String> list = itemListesi(key3);
        for (int i = 0; i < list.size(); i++) {
            if (text.equals(list.get(i))) {
                clickListElement(key2, i);
                System.out.println("Elemente Tiklandi");
            }
        }
    }
}