import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.AfterSuite;
import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.BeforeSuite;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class BaseTest {

    @BeforeSuite
    public void hazirlik() {
        System.out.println("Senaryo Basladi");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        action = new Actions(driver);
        driver.manage().window().maximize();
    }

    static WebDriver driver;
    static Actions action;

    public WebElement findElement(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
        WebElement webElement = webDriverWait
                .until(ExpectedConditions.presenceOfElementLocated(infoParam));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                webElement);
        return webElement;
    }

    public List<WebElement> findElements(String key){
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        return driver.findElements(infoParam);
    }
    public  void sendkeysElement(String by,String text){
        findElement(by).sendKeys(text);
    }

    public void hoverElement(String by) {

        action.moveToElement(findElement(by)).build().perform();
    }

    public void assertControl(String assertName, String expectedName){
        String assertName1 = findElement(assertName).getText();
        System.out.println(assertName1);
        Assert.assertEquals(assertName1,expectedName);
    }

    public void clickElement(String by){
        findElement(by).click();
    }

    public String textElements(String by,int index) {
        String text =findElements(by).get(index).getText();
        return text;
    }

    public void clickListElement(String by,int index){
        findElements(by).get(index).click();
    }

    public void enterClick(String by){
        findElement(by).sendKeys(Keys.ENTER);
    }

    public void assertControls(String assertName, String expectedName){
        List<String> list = itemListesi(assertName);
        Assert.assertTrue("Element var"+expectedName,list.contains(expectedName));
    }

    public List<String> itemListesi(String key){
        List<WebElement> todoList = findElements(key);
        List<String> list = new ArrayList<>();
        for (WebElement i: todoList) {
            list.add(i.getText());
        }
        return list;
    }

    public void assertAttributeControls(String assertName){
        System.out.println(assertName);
        Assert.assertEquals(assertName,"todo completed");
        System.out.println("Mark Edildi");
    }

    public  boolean markEtmek(String key1, String key2, String key3){
        System.out.println("<*>");
        WebElement todo=findElement(key2);
        System.out.println("*");
        List<WebElement> todoList = todo.findElements(By.tagName("li"));
        System.out.println("**");
        List<String> list= itemListesi(key3);
        System.out.println("***");
        int index = list.indexOf(key1);
        System.out.println("****"+index);
        String textmark = todoList.get(index).getAttribute("class");
        System.out.println("*****"+textmark);
        String markE = "todo completed";
        System.out.println("******");
        return (textmark.equals(markE));
    }

    public void newtab(){
        for (String sekme: driver.getWindowHandles()){
            driver.switchTo().window(sekme);
        }
    }

    @AfterSuite
    public void bitir() {
        driver.quit();
        System.out.println("Senaryo Sonlandi");
    }
}